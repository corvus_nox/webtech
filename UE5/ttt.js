var playerSymbol = 'X';
var player2Symbol = 'O';
var win, turn, row, column;
$(document).ready(function() {
  restartGame();
  $("#restart").on("click", function() {
    restartGame();
  });
  $(".cell").on("click", function() {
    if(!win && this.innerHTML === "") {
      if(turn%2 === 0)
        insertSymbol(this, playerSymbol);
      else
      insertSymbol(this, player2Symbol);
    }
  });
});
function insertSymbol(element, symbol) {
  element.innerHTML = symbol;
  if(symbol === player2Symbol)
    $("#" + element.id).addClass("player-two");
  $("#" + element.id).addClass("cannotuse");
  checkWinConditions(element);
  turn++;
  if(win || turn > 8) {
    $("#restart").addClass("btn-green");
    $(".cell").addClass("cannotuse");
  }
}
function restartGame() {
  turn = 0;
  win = false;
  $(".cell").text("");
  $(".cell").removeClass("wincell");
  $(".cell").removeClass("cannotuse");
  $(".cell").removeClass("player-two");
  $("#restart").removeClass("btn-green");
}

function checkWinConditions(element) {
  row = element.id[4];
  column = element.id[5];

  // vertical check
  win = true;
  for(var i=0; i<3; i++)
    if($("#cell" + i + column).text() !== element.innerHTML)
      win = false;

  if(win){
    for(var i=0; i<3; i++)
      $("#cell" + i + column).addClass("wincell");
    return;
  }

  // horizontal check
  win = true;
  for(var i=0; i<3; i++)
    if($("#cell" + row + i).text() !== element.innerHTML)
      win = false;

  if(win) {
    for(var i=0; i<3; i++)
      $("#cell" + row + i).addClass("wincell");
    return;
  }
  // diagonal
  win = true;
  for(var i=0; i<3; i++)
    if($("#cell" + i + i).text() !== element.innerHTML)
      win = false;
  if(win) {
    for(var i=0; i<3; i++)
      $("#cell" + i + i).addClass("wincell");
    return;
  }
  win = false;
  if($("#cell02").text() === element.innerHTML)
    if($("#cell11").text() === element.innerHTML)
      if($("#cell20").text() === element.innerHTML) {
        win = true;
        $("#cell02").addClass("wincell");
        $("#cell11").addClass("wincell");
        $("#cell20").addClass("wincell");
      }
}
