function addTodoItem() {
  var todoItem = $("#new-todo-item").val();
  $("#todo-list").append("<li class='list-group-item'>"+
                          "<label>" + todoItem + "</label>" +
                          " <button class='todo-item-delete btn btn-danger float-right'> Delete</button></li>");
 $("#new-todo-item").val("");
}
function deleteTodoItem(e, item) {
  e.preventDefault();
    $(item).parent().remove();
}
$(function() {
   $("#add-todo-item").on('click', function(e){
     e.preventDefault();
     addTodoItem()
   });
  $("#todo-list").on('click', '.todo-item-delete', function(e){
    var item = this;
    deleteTodoItem(e, item);
  });
});
